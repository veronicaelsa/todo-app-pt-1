class MyComponent extends Component {
    state = {
      accounts: [{ id: 2938 }, { id: 3874 }, { id: 6984 }]
    }
  
    handleDelete = (event, accountId) => {
      const newAccounts = this.state.accounts.filter(
        account => account.id !== accountId
      )
      this.setState({ accounts: newAccounts })
    }
  
    render () {
      return (
        <React.Fragment>
          <h1>Active Accounts</h1>
          {this.state.accounts.map(account => (
            <div>
              <p>Account: {account.id}</p>
              <button onClick={event => this.handleDelete(event, account.id)}>
                Delete Account
              </button>
            </div>
          ))}
        </React.Fragment>
      )
    }
  }
  