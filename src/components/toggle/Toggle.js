class MyComponent extends Component {
  state = {
    on: false
  }

  handleToggle = event => {
    this.setState(state => ({
      on: !state.on;
    }));
  };

  render() {
    return (
      <React.Fragment>
        <h1>Toggle is {this.state.on ? "on" : "off"}</h1>
        <button onClick={this.handleToggle}>Click Me</button>
      </React.Fragment>
    );
  }
}
