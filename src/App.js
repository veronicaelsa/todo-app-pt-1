import React, { Component } from "react";
import todosList from "./todos.json";



class App extends Component {
//items is aTodo
  //addItems is newTodo
  state = {
    todos: todosList,
  };

  


  handleToggle = (event, todoToggleId) => {
    let newTodos = this.state.todos.slice()

    let newNewTodos = newTodos.map((todo) => {
      if(todo.id === todoToggleId){
        todo.completed = !todo.completed
        
      }
      return todo
  })
    this.setState({
      todos: newNewTodos
    });
  };
 
  
  handleDestroy = (event, todoId) => {

    let newTodos = this.state.todos.slice()

    let onlyThese = newTodos.filter(
      todo => todo.id !== todoId 
  )
    this.setState({
      todos: onlyThese
    });
  };

  handleClear = (event) => {
    let setTodos = this.state.todos.slice()

    let notCompleted = setTodos.filter(
      todo => todo.completed !== true
    )
    this.setState({
      todos: notCompleted
    });
  };

 


  handleAdd = (event) => {
    event.preventDefault();
    let newTodo = {
      userId: 785,
      id: Date.now(),
      title: this.newTodo.value,
      completed: false,
    }

    this.newTodo.value = "";

    // let setTodos = this.state.todos.slice()
    // let newTodo = this.newTodo.value;
    
    // let withAdded = setTodos.filter(
    //   todo => todo.completed !== true
    // )
  //   this.setState({
  //     todos: withAdded
  //   });
  // };
    // console.log({newTodo})
    // this.setState({
    //   todos: mappingTodos
    // });
 
    console.log({newTodo})
    this.setState({
      todos: [...this.state.todos, newTodo]
    });
  };




  
  render() {
    
    
    return (
      <section className="todoapp">
        <header className="header">
        <h1>Todos</h1>
        <form onSubmit={this.handleAdd} >
          <input 
              ref={(input)=> {this.newTodo = input}}
              className="new-todo"
              type="text"
              placeholder="What needs to be done?" 
              autoFocus 
            />
        </form>
        </header>
        <TodoList 
          todos={this.state.todos}
          id={this.state.id}
          handleToggle={this.handleToggle}
          handleDestroy={this.handleDestroy}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.id}</strong> item(s) left
          </span>
          <button 
            className="clear-completed" 
            onClick={this.handleClear}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
            className="toggle" 
            type="checkbox" 
            onChange={(event) => 
              this.props.handleToggle(event, this.props.todoId)
            }
            checked={this.props.completed}
          />
          <label>{this.props.title}</label>
          <button 
            className="destroy" 
            onClick={event => 
              this.props.handleDestroy(event, this.props.todoId)
            }
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              title={todo.title} 
              completed={todo.completed} 
              key={todo.id}
              handleToggle={this.props.handleToggle}
              handleDestroy={this.props.handleDestroy}
              todoId={todo.id}
              userId={todo.userId}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
